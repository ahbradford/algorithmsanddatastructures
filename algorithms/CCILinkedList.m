//
//  CCILinkedList.m
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "CCILinkedList.h"
#import "CCICh2Node.h"

@interface CCILinkedList()

@property (nonatomic) NSUInteger count;

@end


@implementation CCILinkedList

-(void)addNode:(int)data
{
    if(!self.headNode)
    {
        self.headNode = [[CCICh2Node alloc]initWithData:data];
        self.headNode.previousNode = nil;
        self.headNode.nextNode = nil;
        self.count = 1;
    }
    else
    {
        CCICh2Node *lastNode = [self lastNodeInList];
        CCICh2Node *nodeToinsert = [[CCICh2Node alloc]initWithData:data];
        lastNode.nextNode = nodeToinsert;
        nodeToinsert.previousNode = lastNode;
        self.count++;
    }
    
}
-(void)insertNodeBeforeNode:(int)data
{
    
}
-(void)insertNodeAfterNode:(int)data
{
    
}
-(void)deleteNode:(CCICh2Node *)nodeToDetete
{
    if(nodeToDetete.previousNode && nodeToDetete.nextNode)
    {
        nodeToDetete.previousNode.nextNode = nodeToDetete.nextNode;
        nodeToDetete.nextNode.previousNode = nodeToDetete.previousNode;
    }
    
    else if(nodeToDetete.nextNode)
    {
        nodeToDetete.nextNode.previousNode = nil;
    }
    else
    {
        nodeToDetete.previousNode.nextNode = nil;
    }
    self.count--;
    
}
-(void)deleteDuplicates
{

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:self.count];
    CCICh2Node *currentNode = self.headNode;
    
    
    while (currentNode)
    {
        BOOL exists = [dict[[NSNumber numberWithInt:currentNode.data]]boolValue];
        if(exists)
        {
            CCICh2Node *nodeToRemove = currentNode;
            currentNode = currentNode.nextNode;
            
            [self deleteNode:nodeToRemove];
        }
        else
        {
            dict[[NSNumber numberWithInt:currentNode.data]] = @YES;
            currentNode = currentNode.nextNode;
        }
    
        
    }

}

-(void)deleteDuplicatesNoBuffer
{
    CCICh2Node *currentNode = self.headNode;
    
    while (currentNode)
    {
        int dataToTest = currentNode.data;
        CCICh2Node *nodeToTest = currentNode.nextNode;
        
        while (nodeToTest) {
            if(nodeToTest.data == dataToTest)
            {
                CCICh2Node *nodeToDelete = nodeToTest;
                nodeToTest = nodeToTest.nextNode;
                [self deleteNode:nodeToDelete];
            }
            else
            {
                nodeToTest = nodeToTest.nextNode;
            }
        }
        currentNode = currentNode.nextNode;
    }
}

-(CCICh2Node *)lastNodeInList
{
    CCICh2Node *result = self.headNode;
    while (result.nextNode) {
        result = result.nextNode;
    }
    return result;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.count = 0;
    }
    return self;
}

-(NSString *)description
{
    
    CCICh2Node *currentNode = self.headNode;
    
    if(!self.headNode)return @"Empty List";
    
    NSMutableString *resultString = [[NSMutableString alloc]initWithCapacity:self.count *4];
    [resultString appendFormat:@"%d",currentNode.data];
    currentNode = currentNode.nextNode;
   
    
    while (currentNode) {
        [resultString appendFormat:@" -> %d",currentNode.data ];
        currentNode = currentNode.nextNode;
    }
    
    return resultString;
    
    
    
}



@end
