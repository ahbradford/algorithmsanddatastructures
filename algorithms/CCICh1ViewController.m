//
//  CCIViewController.m
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "CCICh1ViewController.h"
#import "CCIAlgorithmsCh1Solver.h"

@interface CCICh1ViewController ()

@property (nonatomic,strong) CCIAlgorithmsCh1Solver *solver;

@property (weak, nonatomic) IBOutlet UILabel *question1Label;
@property (weak, nonatomic) IBOutlet UILabel *question1BLabel;



@property (weak, nonatomic) IBOutlet UILabel *question2Label;
@property (weak, nonatomic) IBOutlet UILabel *question3label;
@property (weak, nonatomic) IBOutlet UILabel *question4Label;
@property (weak, nonatomic) IBOutlet UILabel *question5Label;

@property (weak, nonatomic) IBOutlet UITextField *question3TextField;
@property (weak, nonatomic) IBOutlet UITextField *question3TextField2;


@end

@implementation CCICh1ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.solver = [[CCIAlgorithmsCh1Solver alloc]init];
	// Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)question1aEditingDidChange:(UITextField *)sender
{
    BOOL result = [self.solver solve1Q1ForString:sender.text];
    NSString *resultString;
    resultString = (result) ? @"Contains Duplicates" : @"No Duplicates";
    
    self.question1Label.text = resultString;
}
- (IBAction)question1BEditingDidChange:(UITextField *)sender {
    BOOL result = [self.solver solve1Q1BForString:sender.text];
    NSString *resultString;
    resultString = (result) ? @"Contains Duplicates" : @"No Duplicates";
    
    self.question1BLabel.text = resultString;
}
- (IBAction)question2EditingDidChange:(UITextField *)sender
{
    char *result = [self.solver solveQuestion2forString:(char *)[sender.text UTF8String]];
    self.question2Label.text = [NSString stringWithUTF8String:result];
                    
}
- (IBAction)question3EditingDidChange:(UITextField *)sender
{
    BOOL result = [self.solver solveQ3ForString1:self.question3TextField.text string2:self.question3TextField2.text];
   NSString* resultString = (result) ? @"Permutations" : @"No Permutations";
    
    self.question3label.text = resultString;

}
- (IBAction)question4EditingDidChange:(UITextField *)sender
{
    int numberOfSpaces = 0;
    for(int i = 0; i < sender.text.length;i++)
    {
        if([sender.text characterAtIndex:i] == ' ')numberOfSpaces++;
    }
    
    
    
    NSString *resultString = sender.text;
    for(int i = 0; i < numberOfSpaces;i++)
    {
        resultString = [resultString stringByAppendingString:@"11"];
    }
    
    char * result = [self.solver solveQ4ForString:(char *)[resultString UTF8String] length:sender.text.length];
    
    self.question4Label.text = [NSString stringWithUTF8String:result];
    
}
- (IBAction)question5EditingDidChange:(UITextField *)sender
{
    NSString *result = [self.solver solveQ5ForString:sender.text];
    self.question5Label.text = result;
    
}
- (IBAction)testQ6Pressed:(id)sender {
    
    int array[3][3] = {1,2,3,4,5,6,7,8,9};
   
    
    [self.solver rotateMatrix:(int *)array sizeOfM:3];
    
    int array2[4][4] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    
    
    [self.solver rotateMatrix:(int *)array2 sizeOfM:4];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
