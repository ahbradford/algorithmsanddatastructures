//
//  CCIAlgorithmsCh1Solver.h
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCIAlgorithmsCh1Solver : NSObject

-(BOOL)solve1Q1ForString:(NSString *)string;
-(BOOL)solve1Q1BForString:(NSString *)string;
-(BOOL)solveQ3ForString1:(NSString *)string string2:(NSString *)string2;
-(char *)solveQuestion2forString:(char *)string;
-(char *)solveQ4ForString:(char *)string length:(int)length;
-(NSString *)solveQ5ForString:(NSString *)string;
-(void)rotateMatrix:(int*)matrix sizeOfM:(int)sizeOfM;
@end
