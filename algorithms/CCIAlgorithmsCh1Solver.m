//
//  CCIAlgorithmsCh1Solver.m
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "CCIAlgorithmsCh1Solver.h"

@implementation CCIAlgorithmsCh1Solver
-(BOOL)solve1Q1ForString:(NSString *)string
{
    NSMutableDictionary *usedChars = [[NSMutableDictionary alloc]initWithCapacity:string.length];
    
    @autoreleasepool {
        
        for(int i = 0; i < string.length; i++)
        {
            NSString *stringToTest = [string substringWithRange:NSMakeRange(i, 1)];
            BOOL repeated = [usedChars[stringToTest] boolValue];
            
            if(repeated)
            {
                return YES;
            }
            else
            {
                usedChars[stringToTest] = @YES;
            }
            
        }
    }
    return NO;
}

-(BOOL)solve1Q1BForString:(NSString *)string
{
    for(int i = 0; i < string.length;i++)
    {
        char charToTest = [string characterAtIndex:i];
        for(int j = i+1; j < string.length;j++)
        {
            if(charToTest == [string characterAtIndex:j])return YES;
        }
    }
    return NO;
}

-(char *)solveQuestion2forString:(char *)string
{
    int length = strlen(string);
    
    for(int i = 0; i < length/2;i++)
    {
        char firstChar = string[i];
        string[i] = string[length-1-i];
        string[length - 1 - i] = firstChar;
    }
    
    return string;
}
-(BOOL)solveQ3ForString1:(NSString *)string1 string2:(NSString *)string2
{
    if(string1.length != string2.length)return NO;
    
    NSMutableDictionary *dictionary1 = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dictionary2 = [[NSMutableDictionary alloc]init];
    
    for(int i = 0; i < string1.length; i++)
    {
        NSString *string1Temp = [string1 substringWithRange:NSMakeRange(i, 1)];
        NSString *string2Temp = [string2 substringWithRange:NSMakeRange(i, 1)];
        
        NSNumber *number1Temp = dictionary1[string1Temp];
        if(number1Temp) dictionary1[string1Temp] = [NSNumber numberWithInt:number1Temp.intValue + 1];
        else dictionary1[string1Temp] = @1;
        
        NSNumber *number2Temp = dictionary2[string2Temp];
        if(number2Temp) dictionary2[string2Temp] = [NSNumber numberWithInt:number2Temp.intValue + 1];
        else dictionary2[string2Temp] = @1;
        
    }
        
    for(NSString *key in [dictionary1 allKeys])
    {
        NSNumber *value1 = dictionary1[key];
        NSNumber *value2 = dictionary2[key];
        
        if(value1.intValue != value2.intValue) return NO;
    }
    
    return YES;
    
}

-(char *)solveQ4ForString:(char *)string length:(int)length
{
    
    //find out how many spaces we have;
    int numberOfSpaces = 0;
    for(int i = 0; i < length; i++)
    {
        if(string[i] == ' ') numberOfSpaces++;
    }
    
    int totalLength = length + numberOfSpaces *2;
    
    for(int i = length -1; i >=0; i--)
    {
        if(string[i] == ' ')
        {
            string[--totalLength] = '0';
            string[--totalLength] = '2';
            string[--totalLength] = '%';
        }
        else string[--totalLength] = string[i];
    }
    
    return string;
}

-(NSString *)solveQ5ForString:(NSString *)string
{
    if(string.length < 3)return string;
    
    NSMutableString *resultString = [[NSMutableString alloc]initWithCapacity:string.length];
    
    int currentRepeatCount = 1;
    char currentRepeatCharacter = [string characterAtIndex:0];
    for(int i = 1; i < string.length; i++)
    {
        if([string characterAtIndex:i] == currentRepeatCharacter)
            currentRepeatCount++;
        else
        {
            
            
            [resultString appendFormat:@"%c%d",currentRepeatCharacter,currentRepeatCount];
            if(resultString.length >= string.length)
                return string;
            
            currentRepeatCharacter = [string characterAtIndex:i];
            currentRepeatCount = 1;
        }
    }
    [resultString appendFormat:@"%c%d",currentRepeatCharacter,currentRepeatCount];
    return resultString;
}

-(void)rotateMatrix:(int*)theMatrix sizeOfM:(int)m
{
    int (*matrix)[m] = (int (*)[m]) theMatrix;
    [self printMatrix:(int*)matrix sizeOfM:m];
    for(int layer = 0; layer < m/2; layer++)
    {
        int first = layer;
        int last = m - 1 -layer;
        for(int i = first; i< last; i++)
        {
            int offset = i - first;
            int temp = matrix[first][i];
            
            //bottom left to top left
            matrix[first][i] = matrix[last-offset][first];
            
            matrix[last-offset][first] = matrix[last][last-offset];
            
            matrix[last][last - offset] = matrix[i][last];
            
            matrix[i][last] = temp;
            
            
        }
    }
    [self printMatrix:(int *)matrix sizeOfM:m];
}

-(void)printMatrix:(int *)theMatrix sizeOfM:(int)m
{
   int (*matrix)[m] = (int (*)[m]) theMatrix;
    for(int i = 0; i < m;i++)
    {
        for(int j = 0; j < m; j++)
        {
            printf("%02d ",matrix[i][j]);
            
        }
        printf("\n");
    }
    printf("\n");
}

@end
