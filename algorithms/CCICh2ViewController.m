//
//  CCICh2ViewController.m
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "CCICh2ViewController.h"
#import "CCIAlgorithmsCh2Solver.h"
#import "CCILinkedList.h"

@interface CCICh2ViewController ()
@property (strong,nonatomic) CCIAlgorithmsCh2Solver *solver;

@property (weak, nonatomic) IBOutlet UITextView *q1TextView;
@property (weak, nonatomic) IBOutlet UITextView *q1BTextView;



@end

@implementation CCICh2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.solver = [[CCIAlgorithmsCh2Solver alloc]init];
	// Do any additional setup after loading the view.
}


- (IBAction)q1AButtonPressed:(id)sender
{
    //make a test list with dupes at the begining and end
    CCILinkedList *list = [[CCILinkedList alloc]init];
    for(int i = 0; i < 10; i++)
    {
        [list addNode:i];
        if(i%3 == 0)[list addNode:i];
    }
    //print out the list
    self.q1TextView.text = list.description;
    
    //remove dupes
    [list deleteDuplicates];
    
    //append the new result
    self.q1TextView.text = [self.q1TextView.text stringByAppendingFormat:@"\n%@",list.description];
    
    
}
//test

- (IBAction)q1BButtonPressed:(id)sender
{
    //make a test list with dupes at the begining and end
    CCILinkedList *list = [[CCILinkedList alloc]init];
    for(int i = 0; i < 10; i++)
    {
        [list addNode:i];
        if(i%3 == 0)[list addNode:i];
    }
    
    //print out the list
    self.q1BTextView.text = list.description;
    
    //remove duplicates with the special method
    [list deleteDuplicatesNoBuffer];
    
    //print out result again!
    self.q1BTextView.text = [self.q1BTextView.text stringByAppendingFormat:@"\n%@",list.description];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
