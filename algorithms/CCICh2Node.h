//
//  CCICh2Node.h
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCICh2Node : NSObject

@property (nonatomic) int data;
@property (strong,nonatomic) CCICh2Node *previousNode;
@property (strong,nonatomic) CCICh2Node *nextNode;

-(instancetype)initWithData:(int)data;
@end
