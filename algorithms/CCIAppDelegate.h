//
//  CCIAppDelegate.h
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
