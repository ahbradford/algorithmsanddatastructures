//
//  CCICh2Node.m
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "CCICh2Node.h"

@implementation CCICh2Node



-(instancetype)initWithData:(int)data
{
    self =  [super init];
    if(self)
    {
        _data = data;
    }
    return self;
}

-(void)dealloc
{
    NSLog(@"Destroyed Node With Data %d",self.data);
}
@end
