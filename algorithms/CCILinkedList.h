//
//  CCILinkedList.h
//  algorithms
//
//  Created by Adam Bradford on 12/29/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCICh2Node.h"

@interface CCILinkedList : NSObject

@property (strong,nonatomic) CCICh2Node *headNode;

-(void)addNode:(int)data;
-(void)insertNodeBeforeNode:(int)data;
-(void)insertNodeAfterNode:(int)data;
-(void)deleteDuplicates;
-(void)deleteDuplicatesNoBuffer;

@end
